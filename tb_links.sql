-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2021 at 07:46 PM
-- Server version: 8.0.25
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpsamples`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_links`
--

CREATE TABLE `tb_links` (
  `id` int NOT NULL,
  `create_at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `SKU` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `price` int NOT NULL,
  `dvd` varchar(64) DEFAULT '0',
  `furniture` varchar(64) DEFAULT '0',
  `book` varchar(64) DEFAULT '0',
  `producttype` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_links`
--

INSERT INTO `tb_links` (`id`, `SKU`, `name`, `price`, `dvd`, `furniture`, `book`, `producttype`) VALUES
(161, 'JVC200123', 'Acme disc', 1, '700', 'xx', '', 'dvd'),
(164, 'JVC200124', 'Acme disc', 153, '15', 'xx', '', 'dvd'),
(165, 'JVC200125', 'Acme disc', 54, '3', 'xx', '', 'dvd'),
(166, 'GGWP007', 'War and Peace', 43, '', 'xx', '3', 'book'),
(167, 'GGWP008', 'War and Peace', 34, '', 'xx', '65', 'book'),
(168, 'GGWP009', 'War and Peace', 345, '', 'xx', '54', 'book'),
(169, 'TR120555', 'Chair', 40, '', '24x45x15', '', 'furniture'),
(171, 'TR1205556', 'Chair', 34, '', '345x34x43', '', 'furniture');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_links`
--
ALTER TABLE `tb_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `SKU` (`SKU`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_links`
--
ALTER TABLE `tb_links`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
