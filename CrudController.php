<?php
include_once 'Dao.php';

class CrudController
{

    /* Fetch All */
    public function readData()
    {
        try {
            
            $dao = new Dao();
            
            $conn = $dao->openConnection();
            
            $sql = "SELECT * FROM `tb_links` ORDER BY id DESC";
            
            $resource = $conn->query($sql);
            
            $result = $resource->fetchAll(PDO::FETCH_ASSOC);
            
            $dao->closeConnection();
        } catch (PDOException $e) {
            
            echo "There is some problem in connection: " . $e->getMessage();
        }
        if (! empty($result)) {
            return $result;
        }
    }


    /* Add New Record */
    public function add($formArray)
    {
        $height = $_POST['height'];
        $width = $_POST['width'];
        $length = $_POST['length'];
        $sku = $_POST['sku'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $dvd = $_POST['size'];
        $book = $_POST['weight'];
        $furniture = $height."x".$width."x".$length;
        $productType = $_POST['option_kategori'];
        $dao = new Dao();
        
        $conn = $dao->openConnection();
        
        $sql = "INSERT INTO `tb_links`(`sku`, `name`, `price`, `book`, `dvd`,  `furniture`,  `producttype` ) VALUES ('" . $sku . "','" . $name . "','" . $price . "','" . $book . "','" . $dvd . "','" . $furniture . "','" . $productType . "')";
        $conn->query($sql);
        $dao->closeConnection();
    }


    /* Delete a Record */
    public function delete($deleteId)
    {
        $dao = new Dao();
        
        $conn = $dao->openConnection();
        $deleteId = implode(',', $deleteId);
        $sql = "DELETE FROM `tb_links` WHERE id IN($deleteId)";
        
        $conn->query($sql);
        if ($conn == true) {
            echo 1;
        }else{
            echo 0;
        }
        $dao->closeConnection();
    }
}

?>