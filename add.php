<?php
try {
    if (isset($_POST["submit"])) {
        include_once 'CrudController.php';
        $crudcontroller = new CrudController();
        $result = $crudcontroller->add($_POST);
        header("Location: index.php");
}
}catch(PDOException $e){
   $warring = "Please, provide the data of indicated type"  ;
}

?>
<html>
<head>
<title>CRUD</title>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles.css">
<script src="crudEvent.js"></script>

</head>
<body>


<form action="" id="product_form" method="POST" enctype="multipart/form-data" >
<?php 
if(!empty($warring)){
echo '<center>' . $warring . '</center>';
}
?>

<div class="header" style="border-bottom:1px solid black; padding-bottom:15px" >
        <a class="headname">Product Add</a>
                <button type="button"  style="float:right; margin-right: 15px"><a href="index.php" class="add">Cancel</a></button>
        <button type="submit" value="Submit" name="submit" style="float:right; margin-right: 15px" class="add"> Save</button>

        <br>
</div>



<ul class="form-style-1">
    <li>
    <label for="sku">SKU</label>
      <input class="inp" type="text" name="sku" id="sku" placeholder="SKU" required>
    </li>

    <li>
    <label for="name">Name</label>
      <input class="inp" type="text" name="name" id="name" placeholder="Name" required>
    </li>

    <li>
    <label for="price">Price($)</label>
      <input class="inp" type="text" name="price" id="price" placeholder="Price" required>
    </li>

    <li>
    <br>
        <select name="option_kategori" id="productType" title="Kategori" class="selectpicker" data-width="15%">
          <option value="dvd" selected>DVD</option>
          <option value="book">Book</option>
          <option value="furniture">Furniture</option>
        </select>
    </li>

    <li>
    <div class="dvd" id="dvd">
          <input type="text" name="size" id="size" placeholder="size in MB" required>
          <p>Please provide size in MB</p>
        </div>

        <div class="book" id="book" name="book">
          <input type="text" name="weight" id="weight" placeholder="weight in KG">
          <p>Please provide weight in KG</p>
        </div>      

        <div class="furniture" id="furniture" name="furniture">
          <input class="fur" type="text" name="height" id="height" placeholder="Height">
          <input class="fur" type="text" name="width" id="width" placeholder="Width">
          <input class="fur" type="text" name="length" id="length" placeholder="Length">
          <p>Please provide dimensions in HxWxL format</p>
        </div>
    </li>

</ul>



</form>

<footer class="footer">
  <br>
  <center>Scandiweb Test Assigment </center>
  <br>
</footer>



<script>
    $(document).ready(function() {

  $('.book').hide();
  $('.furniture').hide();

  $('[name="option_kategori"]').change(function() {
    if ($('[name="option_kategori"]').val() === "dvd") {
        $('.dvd').show();
        $('.book').hide();
        document.getElementById('weight').value = '';
        $('.furniture').hide();
        document.getElementById('height').value = '';
        document.getElementById('width').value = '';
        document.getElementById('length').value = '';
        document.getElementById('size').required = true;
        document.getElementById('height').required = false;
        document.getElementById('width').required = false;
        document.getElementById('length').required = false;
        document.getElementById('weight').required = false;
    }
    if ($('[name="option_kategori"]').val() === "book") {
        $('.book').show();
        $('.dvd').hide();
        document.getElementById('size').value = '';
        $('.furniture').hide();
        document.getElementById('height').value = '';
        document.getElementById('width').value = '';
        document.getElementById('length').value = '';
        document.getElementById('weight').required = true;
        document.getElementById('height').required = false;
        document.getElementById('width').required = false;
        document.getElementById('length').required = false;
        document.getElementById('size').required = false;
    }
    if ($('[name="option_kategori"]').val() === "furniture") {
        $('.furniture').show();
        $('.dvd').hide();
        document.getElementById('size').value = '';
        $('.book').hide();
        document.getElementById('weight').value = '';
        document.getElementById('height').required = true;
        document.getElementById('width').required = true;
        document.getElementById('length').required = true;
        document.getElementById('size').required = false;
        document.getElementById('weight').required = false;

    }
  })
});
</script>





</body>
</html>


